const  moment = require('moment');
function mode(numbers) {
    // as result can be bimodal or multi-modal,
    // the returned result is provided as an array
    // mode of [3, 5, 4, 4, 1, 1, 2, 3] = [1, 3, 4]
    var modes = [], count = [], i, number, maxIndex = 0;
  
    for (i = 0; i < numbers.length; i += 1) {
        number = numbers[i];
        count[number] = (count[number] || 0) + 1;
        if (count[number] > maxIndex) {
            maxIndex = count[number];
        }
    }
  
    for (i in count)
        if (count.hasOwnProperty(i)) {
            if (count[i] === maxIndex) {
                modes.push(Number(i));
            }
        }
  
    return modes;
  }

  function statePorce(state){
     var stateFinal = {"feliz":0,"neutral":0,"triste":0,"rabia":0,"sorprendido":0,"miedo":0,"disgusto":0} 
     var totalsum = state[0]["1"]+state[0]["2"]+state[0]["3"]+state[0]["4"]+state[0]["5"]+state[0]["0"]+state[0]["6"]

    //  if(totalsum!=0 ){
     
    //  }else{
    //      return stateFinal
    //  }

     stateFinal.triste=(state[0]["1"]/totalsum)*100
     stateFinal.feliz=(state[0]["2"]/totalsum)*100
     stateFinal.rabia=(state[0]["3"]/totalsum)*100  
     stateFinal.sorprendido=(state[0]["4"]/totalsum)*100
     stateFinal.miedo=(state[0]["5"]/totalsum)*100
     stateFinal.neutral=(state[0]["0"]/totalsum)*100
     stateFinal.disgusto=(state[0]["6"]/totalsum)*100
     return stateFinal
   
  }

  function totalGender(genders){
     
    var men = 0;
    var woman = 0;      
    for(var i=0;i<genders.length;i++){
       
        if(genders[i]==1){
            men=men+1
        }else{
            woman=woman+1
        }
    }
    return({men,woman}) 
  }
  

function cleanDataAnalytic (data, idData,genData,emotData,ageData){
var loss=[]
var newDataId =[]
var send=[]
var genderSend=[]
var emotionSend=[]
var ageSend=[]
var newData = []
var index = 0
var nuResiduo=0
let newDatatime
/*
    Comparo el nuevo los nuevos ids con los ids anteriores, y extraigo las posiciones que son 
    diferentes del ids anteriores ya que indican que no están.
*/
var idtemp = data.map((obj,key)=>{return obj.id})
/* Sacando los items */
data.map((obj,key)=>{
    if(!idData.includes(obj.id))
    {
    loss.push(key)
    }else{
        /*Agregar objecto de edad */
        obj.age.push(ageData[key])
        obj.gender.push(genData[key])
        obj.emotion[0][String(emotData[key])]++
        // console.log("GENDER ::> ",obj.gender," Emmotion ::> ",obj.emotion[0])
        // if(genData[key]!="" &&  emotData[key]!="" && genData[key]!=undefined &&  emotData[key]!=undefined && genData[key]!=null &&  emotData[key]!=null   ){
         
        // }
     
    }
   
})
/* iniciando despues de la primera vez los items */
for(var i=0;i<idData.length;i++){
    if(!idtemp.includes(idData[i])){
        var temp = {
            id :idData[i],
            start:moment().subtract(5, 'hours'),
            gender:[],
            emotion:[{"1":0,"2":0,"3":0,"4":0,"5":0,"0":0,"6":0}],
            age:[]
        }
    //    console.log("Inicializando variable Id",temp.id," Hora es igual",temp.start)
        temp.age.push(ageData[i])
        temp.gender.push(genData[i])
        temp.emotion[0][String(emotData[i])]=1
        // console.log("antes de agregarlo ", temp)
        newData.push(temp)
    }
} 
/*
Se calcula el tiempo en minutos o segundos que dura un objeto y separando por frnaja de hora 
*/
for(var i=0;i<loss.length;i++){
    index=loss[i]
    data[index].end=moment().subtract(5, 'hours')
    ageSend.push({age:mode(data[index].age)[0],start:data[index].start,end:data[index].end})
    genderSend.push({gender:mode(data[index].gender)[0],start:data[index].start,end:data[index].end})
    emotionSend.push({emotion:statePorce(data[index].emotion),start:data[index].start,end:data[index].end})
    nuHour= data[index].end.diff(data[index].start,'h');
    newDatatime=data[index].start.clone()
    send.push({
        start:data[index].start,
        end:data[index].end,
        duration:(data[index].end.diff(data[index].start, 's'))/60
    })

    }

for(var i=0;i<data.length;i++){
   if(!loss.includes(i)){
    newData.push(data[i])
    newDataId.push(data[i].id)
   }  
}

// for(var i=0 ; i< idData.length;i++){
//     if(!newDataId.includes(idData[i])){
//         newData.push({
//             id:idData[i],
//             start:moment(),
//             gender:[genData[i]],
//             emotion:[emotData[i]]
//         })
//     }
// }

return {newData,send,genderSend,emotionSend,ageSend}
}

module.exports={
    totalGender,
    cleanDataAnalytic
}


