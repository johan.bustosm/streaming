// var data = ["m","m","h"]
// var copy = data.slice()
// var mode = []
// for(var i=0 ; i<data.length;i++){
//     var key = data[i]
//     console.log(key)
//     var temp = []
//     var count=0
//     console.log("copy",copy.length)
//     for(var j = 0 ; j<copy.length;j++){
//         // console.log(copy[j])
//         if(key===copy[j]){
//             count++
//         }else{
//             temp.push(copy[j]) 
//         }
//     }
//     copy = temp.slice()
//     mode.push(count)

// }

// console.log("mode :",mode.sort())

/* */

function mode(numbers) {
    // as result can be bimodal or multi-modal,
    // the returned result is provided as an array
    // mode of [3, 5, 4, 4, 1, 1, 2, 3] = [1, 3, 4]
    var modes = [], count = [], i, number, maxIndex = 0;
 
    for (i = 0; i < numbers.length; i += 1) {
        number = numbers[i];
        count[number] = (count[number] || 0) + 1;
        if (count[number] > maxIndex) {
            maxIndex = count[number];
        }
    }
 
    for (i in count)
        if (count.hasOwnProperty(i)) {
            if (count[i] === maxIndex) {
                modes.push(Number(i));
            }
        }
 
    return modes;
}
console.log(mode([1,1,2,2,3,3,3]))