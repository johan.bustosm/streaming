var port = process.argv[2]
var camera = process.argv[3]
var app = require('express')();
var http = require('http').Server(app);
var io = require('socket.io')(http);
var moment = require('moment');
var request = require('superagent');
var analytic = require('./analytic.js');
var env = require("./env");
var fechainit = moment({
  hour: 00,
  minute: 00
}).format("YYYY-MM-DD");
var historyIn = 0;
var historyOut = 0;
var listobj = []
var listobjCountingObject = []
var listobjIncomegObject = []
var timeglobalStreaming = moment()
var timeglobalAI = moment()
var flagphoto = false

/* Alerta de streaming desde deskt */
var timeAlertStreaming = 300 // 5m -> 300s
var flagAlert = false

function cleanData(data, recive) {
  var loss = []
  var newDataId = []
  var send = []
  var newData = []
  var index = 0
  var nuResiduo = 0
  let newDatatime
  /*
      Comparo el nuevo los nuevos ids con los ids anteriores, y extraigo las posiciones que son 
      diferentes del ids anteriores ya que indican que no están.
  */
  data.map((obj, key) => {
    if (!recive.includes(obj.id)) {
      loss.push(key)
    }
  })
  /*
  Se calcula el tiempo en minutos que duro el objeto y separando por frnaja de hora 
  */
  for (var i = 0; i < loss.length; i++) {
    index = loss[i]
    data[index].end = moment().subtract(5, 'hours')
    nuHour = data[index].end.diff(data[index].start, 'h');
    newDatatime = data[index].start.clone()
    if (nuHour > 1) {
      for (var i = 0; i <= nuHour; i++) {
        if (i == nuHour) {
          nuResiduo = data[index].end.get('minute');
          send.push({
            start: data[index].end,
            duration: nuResiduo
          })
        } else {
          newDatatime = newDatatime.add(nuResiduo, 'm');
          nuResiduo = 60 - newDatatime.get('minute');
          send.push({
            start: newDatatime.clone(),
            duration: nuResiduo
          })
        }
      }
    } else {
      send.push({
        start: data[index].start,
        duration: (data[index].end.diff(data[index].start, 's')) / 60
      })
    }
  }

  for (var i = 0; i < data.length; i++) {
    if (!loss.includes(i)) {
      newData.push(data[i])
      newDataId.push(data[i].id)
    }
  }

  for (var i = 0; i < recive.length; i++) {
    if (!newDataId.includes(recive[i])) {
      newData.push({
        id: recive[i],
        start: moment().subtract(5, 'hours')
      })
    }
  }

  return {
    newData,
    send
  }
}


function mode(numbers) {
  // as result can be bimodal or multi-modal,
  // the returned result is provided as an array
  // mode of [3, 5, 4, 4, 1, 1, 2, 3] = [1, 3, 4]
  var modes = [],
    count = [],
    i, number, maxIndex = 0;

  for (i = 0; i < numbers.length; i += 1) {
    number = numbers[i];
    count[number] = (count[number] || 0) + 1;
    if (count[number] > maxIndex) {
      maxIndex = count[number];
    }
  }

  for (i in count)
    if (count.hasOwnProperty(i)) {
      if (count[i] === maxIndex) {
        modes.push(Number(i));
      }
    }

  return modes;
}



setInterval(() => {
  try {
    var deltaTimeStreaming = moment().diff(timeglobalStreaming, 's')
    var deltaTimeIA = moment().diff(timeglobalAI, 's')
    /*
      state 1 -> gris (0,0) no hay trasmision desde Desk a server
      state 2 -> azul (1,0) no hay trasmision desde AI a server
      state 3 -> rojo (1,1) funcionando
     */
    // console.log("Time (s)",deltaTimeStreaming)
    if(deltaTimeStreaming >= timeAlertStreaming ){
      if(!flagAlert){
        request
        .post(env.urlStatusIa)
        .send({
          camera: camera,
          status: "off",
          port:port
        })
        .end((err, res) => { 
          
          if(res.status==200){
            flagAlert = true
            io.of('/brows').emit('status-frame',{state:0})
          }
        });
      }
    } else if (deltaTimeStreaming > 2 && deltaTimeIA > 2) {
       io.of('/brows').emit('time', { state: 1 })
    } else if (deltaTimeStreaming < 2 && deltaTimeIA > 2) {
       io.of('/brows').emit('time', { state: 2 })
    } else if (!deltaTimeStreaming < 2 && deltaTimeIA < 2) {
       io.of('/brows').emit('time', { state: 3 })
    } else {
       io.of('/brows').emit('time', { state: 1 })
    }


  } catch (error) {

  }
}, 5000);

app.get('/', function (req, res) {
  res.sendFile(__dirname + '/public/cli.html');
});

/* Desktop */
var streaming = io.of('/streaming').on('connection', function (strUser) {
  strUser.on('new-frame', frame => {
    if(flagAlert === true){
      flagAlert=false
      request
      .post(env.urlStatusIa)
      .send({
        camera: camera,
        status: "on",
        port:port
      })
      .end((err, res) => { 
        
        if(res.status==200){
          io.of('/brows').emit('status-frame',{state:1})
        }
      });
     
    }   
    timeglobalStreaming = moment()
    io.of('/ia').emit('frame', frame)
  });
})

/* Broswer */

var brows = io.of('/brows').on('connection', function (browsUser) {
  browsUser.on('streaming', status => {
    flagphoto = !flagphoto
  });
})

/* Artificial intelligence */

var ia = io.of('/ia').on('connection', function (iaUser) {

  iaUser.on('data', data => {
    timeglobalAI = moment()
    if (data.type == "countingp") {
      // data.id=data.id.split(",")
      if (flagphoto) {
        io.of('/brows').emit('count', {
          id: data.idApp,
          result: listobjCountingObject.length,
          frame: data.frame
        });
      } else {
        io.of('/brows').emit('count', {
          id: data.idApp,
          result: listobjCountingObject.length,
        });
      }
      if (listobjCountingObject.length == 0) {

        listobjCountingObject = data.id.map((obj, key) => {

          return {
            id: obj,
            start: moment().subtract(5, 'hours')
          }
        })
      } else {
        var resultCountObject = cleanData(listobjCountingObject, data.id)
        listobjCountingObject = resultCountObject.newData.slice()
        if (resultCountObject.send.length > 0) {

          request
            .post(env.url)
            .send({
              app: data.idApp,
              value: resultCountObject.send
            })
            .end((err, res) => { });
        }
      }
    } else if (data.type == "incomep") {
      var now = moment()
      var duration = moment.duration(now.diff(fechainit));
      var days = duration.asDays();
      var tempValues = []
      if (days == 1) {
        historyIn = 0
        historyOut = 0
      }
      if (data.inState == "true" && data.outState == "true") {
        historyIn = parseInt(historyIn) + parseInt(data.inActual)
        historyOut = parseInt(historyOut) + parseInt(data.outActual)
        tempValues = [{
          "typedate": "in",
          "date": moment().subtract(5, 'hours'),
          "value": parseInt(data.inActual)
        }, {
          "typedate": "out",
          "date": moment().subtract(5, 'hours'),
          "value": parseInt(data.outActual)
        }]
      } else if (data.inState == "true" && data.outState == "false") {
        historyIn = parseInt(historyIn) + parseInt(data.inActual)
        tempValues = [{
          "typedate": "in",
          "date": moment().subtract(5, 'hours'),
          "value": parseInt(data.inActual)
        }]
      } else if (data.outState == "true" && data.inState == "false") {
        historyOut = parseInt(historyOut) + parseInt(data.outActual)
        tempValues = [{
          "typedate": "out",
          "date": moment().subtract(5, 'hours'),
          "value": parseInt(data.outActual)
        }]
      }
      if (flagphoto) {
        io.of('/brows').emit('count', {
          "valueIn": historyIn,
          "valueOut": historyOut,
          "id": data.id,
          "frame": data.frame
        });
      } else {
        io.of('/brows').emit('count', {
          "valueIn": historyIn,
          "valueOut": historyOut,
          "id": data.id
        });
      }

      if (data.inState == "true" || data.outState == "true") {
        request
          .post(env.urlincome)
          .send({
            app: data.id,
            value: tempValues
          })
          .end((err, res) => { });
      }
    } else if (data.type == "analyticp") {


      if (flagphoto) {
        io.of('/brows').emit('count', {
          id: data.idApp,
          result: analytic.totalGender(data.gender),
          frame: data.frame
        });
      } else {
        io.of('/brows').emit('count', {
          id: data.idApp,
          result: analytic.totalGender(data.gender)
        });
      }

      if (listobj.length == 0) {

        listobj = data.id.map((obj, key) => {
          var temp = {
            id: obj,
            start: moment().subtract(5, 'hours'),
            gender: [],
            emotion: [{
              "1": 0,
              "2": 0,
              "3": 0,
              "4": 0,
              "5": 0,
              "0": 0,
              "6": 0
            }],
            age: [],
          }
          temp.age.push(data.age[key])
          temp.gender.push(data.gender[key])
          temp.emotion[0][String(data.emotion[key])] = 1
          return temp
        })
      } else {
        // if (data.id.length != 0) {
        var result = analytic.cleanDataAnalytic(listobj, data.id, data.gender, data.emotion, data.age)
        listobj = result.newData.slice()

        if (result.send.length > 0) {
          request
            .post(env.urlAnalytic)
            .send({
              app: data.idApp,
              data: result.send,
              gender: result.genderSend,
              emotion: result.emotionSend,
              age: result.ageSend
            })
            .end((err, res) => {
              if (err) {
                console.log("Analytic :", err)
              }
            });
        }

        //}

      }
    }
  });
});

http.listen(port, function () {
  console.log('listening on *:', port);
});