from flask import Flask
from flask import request
from flask import jsonify
from subprocess import Popen
import json
launcherProcess = []
cameras = []
app = Flask(__name__)
@app.route('/on', methods=['POST'])
def newStrimg():
    if request.method == 'POST':
        print("New Streaming")
        jsonPayload = json.loads(request.data.decode("utf-8"))
        cameras.append(jsonPayload['camera_id'])
        launcherProcess.append(Popen(['node','../socket-broca/index.js',str(jsonPayload['port']),str(jsonPayload['camera_id'])]))
        # return(str({'code':200,'message':"Streaming On"}))
        message = {
            'code': 200,
            'message': 'Streaming On'}
        resp = jsonify(message)
        resp.status_code = 200
        return resp

@app.route('/off', methods=['POST'])
def deleStrimg():
        print("Delete Streaming")
        jsonPayload = json.loads(request.data.decode("utf-8"))
        index =cameras.index(str(jsonPayload['camera_id']))
        launcherProcess[index].kill()
        del launcherProcess[index]
        del cameras[index]
        # return(str({"code":200,"message":"The Streaming was deleted"}))
        message = {
            'code': 200,
            'message': 'The Streaming was deleted'}
        resp = jsonify(message)
        resp.status_code = 200
        return resp

if __name__=='__main__':
    app.run(debug=True,port=4001,host="0.0.0.0")
